package domain;

public class Teclado extends DispositivosEntrada {

    private final int idTeclado;
    private static int contadorTeclado;

    public Teclado(String entrada, String marca) {
        super(entrada, marca);
        this.idTeclado = ++contadorTeclado;
    }

    @Override
    public String toString() {
        return "Teclado{" + "idTeclado=" + idTeclado + "," + super.toString() + '}';
    }

}
